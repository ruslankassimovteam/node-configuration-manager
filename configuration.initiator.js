"use strict";

var fs = require('fs');
var ConfigurationManager = require('./index.js');

/**
 * Moment timezone
 */
var moment = require('moment-timezone');
moment().tz("Europe/Paris").format();

/**
 * Application configuration
 */
function CONFIGURATION() {

    var Configuration = ConfigurationManager.createNewConfiguration();

    // Global configuration
    ConfigurationManager.setGlobalConfiguration(
        Configuration,
        {
            debug : false

        }
    );

    // Dev environment configuration
    ConfigurationManager.addEnvironmentConfiguration(
        Configuration,
        'dev',
        {
            debug : true
        }
    );

    // Staging environment configuration
    ConfigurationManager.addEnvironmentConfiguration(
        Configuration,
        'staging',
        {
            debug : true
        }
    );

    // Production environment configuration
    ConfigurationManager.addEnvironmentConfiguration(
        Configuration,
        'prod',
        {
            debug : false
        }
    );

    ConfigurationManager.loadEnvironment(Configuration, 'dev');

    if (fs.existsSync(__dirname + '/configuration.local.js')) {
        console.log('Adding local configuration ...');
        require(__dirname + '/configuration.local.js')(Configuration);
    } else {
        console.log('[WARNING] No local configuration  found !');
    }

    return Configuration;
}

module.exports = CONFIGURATION();