"use strict";

/**
 * Configuration manager
 *
 * @constructor
 */
var CONFIGURATION_MANAGER = function () {
    var self = this;

    /**
     * Merge two objects
     *
     * @param target
     * @param src
     *
     * @returns {boolean|Array|*}
     */
    self.mergeObjects = function (target, src) {
        var array = Array.isArray(src);
        var dst = array && [] || target;

        if (target && typeof target === 'object') {
            Object.keys(target).forEach(
                function (key) {
                    dst[key] = target[key];
                }
            )
        }
        Object.keys(src).forEach(
            function (key) {
                if (typeof src[key] !== 'object' || !src[key]) {
                    dst[key] = src[key];
                }
                else {
                    if (!target[key]) {
                        dst[key] = src[key];
                    } else {
                        dst[key] = self.mergeObjects(target[key], src[key]);
                    }
                }
            }
        );

        return dst;
    };

    /**
     * Creates new configuration
     *
     * @returns Configuration
     */
    self.createNewConfiguration = function () {
        var Configuration = {
            _globalConfiguration: {},
            _environmentConfiguration: {},
            _additionalConfigurations: []
        };

        return Configuration;
    };

    /**
     * Adds environment specific configuration
     *
     * @param currentConfiguration
     * @param environment
     * @param environmentConfiguration
     */
    self.addEnvironmentConfiguration = function (currentConfiguration, environment, environmentConfiguration) {
        if (typeof environmentConfiguration === 'object') {
            currentConfiguration._environmentConfiguration[environment] = environmentConfiguration;
        }
    };

    /**
     * Sets global configuration
     *
     * @param currentConfiguration
     * @param globalConfiguration
     */
    self.setGlobalConfiguration = function (currentConfiguration, globalConfiguration) {
        if (typeof globalConfiguration === 'object') {
            currentConfiguration._globalConfiguration = globalConfiguration;
        }
    };

    /**
     * Removes all configuration properties
     *
     * @param currentConfiguration
     */
    self.resetConfiguration = function (currentConfiguration) {
        Object.keys(currentConfiguration).forEach(
            function (key) {
                if (['_environmentConfiguration', '_additionalConfigurations', '_globalConfiguration'].indexOf(key) === -1) {
                    delete currentConfiguration[key];
                }
            }
        )
    };

    /**
     * Loads global configuration
     *
     * @param currentConfiguration
     */
    self.loadGlobal = function (currentConfiguration) {
        self.resetConfiguration(currentConfiguration);
        self.mergeObjects(
            currentConfiguration,
            currentConfiguration._globalConfiguration
        );
    };

    /**
     * Load global, then environment configuration
     *
     * @param currentConfiguration
     * @param environment
     */
    self.loadEnvironment = function (currentConfiguration, environment) {
        self.resetConfiguration(currentConfiguration);
        self.mergeObjects(
            currentConfiguration,
            currentConfiguration._globalConfiguration
        );
        self.mergeObjects(
            currentConfiguration,
            currentConfiguration._environmentConfiguration[environment]
        );
    };

    /**
     * Loads all previously applied additional configuration
     *
     * @param currentConfiguration
     */
    self.reloadAdditionalConfiguration = function (currentConfiguration) {
        for (var i in currentConfiguration._additionalConfigurations) {
            if (currentConfiguration._additionalConfigurations.hasOwnProperty(i)) {
                self.mergeObjects(
                    currentConfiguration,
                    currentConfiguration._additionalConfigurations[i]
                );
            }
        }
    };

    /**
     * Applies (and adds to "cache") additional configuration
     *
     * @param currentConfiguration
     * @param additionalConfiguration
     */
    self.applyConfiguration = function (currentConfiguration, additionalConfiguration) {
        if (typeof additionalConfiguration === 'object') {
            currentConfiguration._additionalConfigurations.push(additionalConfiguration);
            self.mergeObjects(currentConfiguration, additionalConfiguration);
        }
    };
};

module.exports = new CONFIGURATION_MANAGER();