"use strict";

var ConfigurationManager = require('./index.js');

/**
 * Application local configuration
 */
function LOCAL_CONFIGURATION(Configuration) {
    // If you need to reload the environment use this :
    // ConfigurationManager.loadEnvironment(Configuration, 'staging');
    // ConfigurationManager.reloadAdditionalConfiguration(Configuration);

    ConfigurationManager.applyConfiguration(
        Configuration,
        {
            // WRITE YOUR CONFIGURATION HERE
            debug: true
        }
    );
}

module.exports = LOCAL_CONFIGURATION;